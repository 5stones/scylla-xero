from pprint import pprint
from scylla import app
from scylla import configuration
from . import client
from . import records
from . import tasks


class App(app.App):

    # wait 10 minutes between each set of tasks, because of usage limits
    nap_length = 600

    main_options = [
        ('after', 'a', None),
        #('limit', 'l', 100),
        #('page', 'p', 1),
        ('retry-errors', 'r'),
    ]

    def _prepare(self):
        # load config "tasks" which should be a list of collection names to run through
        scheduled_tasks = configuration.get('Xero', 'tasks', [])

        self.client = client.XeroClient()

        # add all possible records as on demand or scheduled tasks
        for name in self.client.collection_names:
            try:
                task = tasks.XeroTask(self.client, name)
                if name in scheduled_tasks:
                    self.tasks[name] = task
                else:
                    self.on_demand_tasks[name] = task
            except TypeError:
                # ignore the end points that don't instanciate; they're probably not records
                pass


if __name__ == '__main__':
    # test the client connection
    xero = client.XeroClient()

    response = xero.contacts.all()
    #response = xero.invoices.all()
    #response = xero.users.all()
    #response = xero.banktransactions.all()
    #response = xero.items.all()

    rec = records.XeroRecord.factory(response[0])
    print(rec.classname, rec.key_field)
    pprint(rec.data)
