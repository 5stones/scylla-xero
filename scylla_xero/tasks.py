from datetime import datetime
from pprint import pprint
from scylla import convert
from scylla import log
from scylla import tasks
from xero.exceptions import XeroBadRequest, XeroNotFound
from . import client
from . import records
from pprint import pprint


class XeroTask(tasks.Task):
    """Downloads records from Xero.
    """

    def __init__(self, client, collection_name):
        self.collection_name = collection_name
        self.client_collection = getattr(client, collection_name)

        self.record_type = self.client_collection.singular
        self.classname = 'Xero' + self.record_type

        task_id = 'Xero.' + collection_name
        super(XeroTask, self).__init__(task_id)

    def _step(self, after, options):
        print("\nGetting {} updated after {} :".format(self.collection_name, after))

        params = { 'page': 1 }
        if after:
            if not isinstance(after, datetime):
                after = convert.parse_iso_date(after)
            params['since'] = after

        response = None
        while response is None or len(response) == 100:
            response = self.client_collection.filter(**params)
            self._process_search_response(response)
            params['page'] += 1

    def _process_search_response(self, response):
        for obj in response:
            # save the each record to orientdb
            self._process_response_record(obj)

    def _process_response_record(self, obj):
        rec = records.XeroRecord.factory(obj, obj_type=self.record_type)
        rec.throw_at_orient()
        print("Saved {} {} as {} {}".format(rec.xero_type, rec.id, rec.classname, rec.rid))

    def process_one(self, rec_id):
        try:
            response = self.client_collection.get(rec_id)
            self._process_search_response(response)
        except XeroNotFound:
            log.print_error('"{}" not found in {}.'.format(rec_id, self.collection_name))


class ToXeroTask(tasks.ReflectTask):
    """Checks for updates to records an pushes them into Xero.
    """
    def __init__(self,
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=None,
            with_reflection=None):

        self.xero_client_collection = xero_client_collection
        self.link_field = to_class[1] + 'ID'
        self.conversion = conversion

        super(ToXeroTask, self).__init__(
            from_class,
            to_class,
            where=where,
            with_reflection=with_reflection)

    def _process_response(self, from_objs):
        """Processes a page of records from orient, and pushes them to Xero.
        """
        if not from_objs or not len(from_objs):
            print('No updated records')
            return
        requests = super(ToXeroTask, self)._process_response(from_objs)
        return self._save_page(from_objs, requests)

    def _process_response_record(self, obj):
        """Creates objs to pass to Xero which will create or update those records.
        """
        data = self.conversion(obj)

        # fill in the ID field
        key = obj.get('_linked_to')
        if key:
            data[self.link_field] = key

        return data

    def _save_page(self, from_objs, to_objs):
        # create arrays so we know which starting object matches a request and response
        sources = []
        requests = []
        for (i, obj) in to_objs.iteritems():
            sources.append(from_objs[i])
            requests.append(obj)

        if not len(requests):
            return

        # save the page of objects to xero
        results = self.xero_client_collection.save(requests, summarize_errors=False)

        # process the resonse and save errors
        for (i, result) in enumerate(results):
            from_obj = sources[i]
            request = requests[i]
            self._save_one_response(result, from_obj, request)

    def _save_one_response(self, result, from_obj, request):
        # catch errors while processing each record result
        with log.ErrorLogging(self.task_id, from_obj):
            is_update = self.link_field in request

            if result['StatusAttributeString'] == 'ERROR':
                self._handle_response_error(result, from_obj, request, is_update)
            else:
                self._handle_response_success(result, from_obj, request, is_update)

    def _handle_response_error(self, result, from_obj, request, is_update):
        # this record failed; format and throw exception
        errors = [error['Message'] for error in result['ValidationErrors']]
        err = Exception(errors)
        err.details = result
        raise err

    def _handle_response_success(self, result, from_obj, request, is_update):
        # this record succeeded; save the response and link it
        result_rec = records.XeroRecord.factory(result, obj_type=self.to_type)
        self._save_response(from_obj, result_rec, is_update=is_update, request=request)
