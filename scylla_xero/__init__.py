"""Core connection with Xero Accounting.

For a testing, create a demo account, and add a private application with credentials:
https://developer.xero.com/documentation/getting-started/development-accounts/

As a private application, this is the guide to create private credentials:
https://developer.xero.com/documentation/advanced-docs/public-private-keypair/

configuration required:
    'Xero': {
        'consumer_key': '',
        'credential_path': '',
        'url': 'https://api.xero.com/api.xro/2.0/',
        'tasks': ['invoices'],
    },
"""

from .app import App

from .client import XeroClient

from .records import XeroRecord

from .tasks import ToXeroTask

from .conversions import XeroConversion
