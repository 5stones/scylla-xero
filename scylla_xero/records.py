from scylla import records


class XeroRecord(records.Record):
    """Manages a Xero record and its subrecords.

    Attributes:
        custom_data: Dictionary created from "nameValues" field containing data outside the normal schema.
    """
    key_field = None
    classname = 'XeroRecord'

    _sub_records = {
        'Contact': 'Contact',
        'Invoice': 'Invoice',
    }

    def __init__(self, xero_type, obj, module='Xero'):
        """Use XeroRecord.factory(obj) instead.
        """
        super(XeroRecord, self).__init__(obj)
        self.module = module
        self.xero_type = xero_type
        self.classname = module + xero_type
        self.key_field = xero_type + 'ID'

        self._process_fields()

    @classmethod
    def factory(cls, obj, module='Xero', obj_type=None):
        """Instantiates a XeroRecord object using a subclass if one is available.
        """
        if not obj_type:
            obj_type = cls.__get_obj_type(obj)
        classname = module + obj_type
        for subclass in cls.__subclasses__():
            if subclass.__name__ == classname:
                return subclass(obj)

        return cls(obj_type, obj)

    @property
    def id(self):
        return self.get(self.key_field)

    @staticmethod
    def __get_obj_type(obj):
        """Figures out the type of an object by which one ends with "ID".
        """
        for (field, value) in obj.iteritems():
            if field.endswith('ID'):
                return field[:-2]
        raise Exception('Could not determine type of record; missing field ending in ID.')

    def _process_fields(self):
        # create sub records (updates the record during save)
        for (field, classname) in self._sub_records.items():
            if field in self.data:
                self.data[field] = XeroRecord.factory(self[field], self.module, obj_type=classname)
