import xero
from scylla import configuration
from xero.auth import PrivateCredentials
from xero.utils import OBJECT_NAMES


# monkey patch the save to allow for summarize_errors=False
def __collection_save(self, data, summarize_errors=True):
    return self.save_or_put(data, method='post', summarize_errors=summarize_errors)
xero.basemanager.BaseManager._save = __collection_save


class XeroClient(xero.Xero):
    config = configuration.getSection('Xero')

    def __init__(self, unit_price_4dps=True):
        # Add BatchPayments to API. Workaround for https://github.com/freakboy3742/pyxero/pull/249
        OBJECT_NAMES['BatchPayments'] = u'BatchPayment'
        self.OBJECT_LIST = xero.Xero.OBJECT_LIST + (
            "BatchPayments",
        )

        super(XeroClient, self).__init__(self.__credentials, unit_price_4dps=unit_price_4dps)
        self.name = self.config.get('name', 'Xero')

    @property
    def __credentials(self):
        consumer_key = self.config['consumer_key']
        with open(self.config['credential_path']) as keyfile:
            rsa_key = keyfile.read()
        return PrivateCredentials(consumer_key, rsa_key)

    @property
    def collection_names(self):
        return sorted([k
            for (k,v) in self.__dict__.items()
            if isinstance(v, xero.manager.Manager)])
