from scylla import convert


class XeroConversion(convert.Conversion):

    def __init__(self, key_field, conversion_map=None, post_convert=None):
        super(XeroConversion, self).__init__(conversion_map, post_convert)
        self.key_field = key_field

    def convert(self, obj, included_fields=None):
        data = super(XeroConversion, self).convert(obj, included_fields)

        # automatically add ID?
        #key = obj.get('_linked_to')
        #if key:
        #    data[self.key_field] = key

        # remove ID if empty
        if self.key_field in data and not data[self.key_field]:
            del data[self.key_field]

        return data

    @classmethod
    def _convert_field(cls, obj, conversion, parent_obj=None):
        value = super(XeroConversion, cls)._convert_field(obj, conversion, parent_obj)
        # None gets converted to "None" with the api
        if value is None:
            return ''
        return value
