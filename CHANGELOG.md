# [1.3.0](https://gitlab.com/5stones/scylla-xero/compare/v1.2.1...v1.3.0) (2019-05-10)


### Features

* **XeroClient:** Add the ability to use 4dp for xero rounding ([f9df453](https://gitlab.com/5stones/scylla-xero/commit/f9df453))



## [1.2.1](https://gitlab.com/5stones/scylla-xero/compare/v1.1.0...v1.2.1) (2019-03-05)


### Bug Fixes

* **XeroRecord, XeroTask, ToXeroTask:** Fix an issue with incorrect inference of factory record type ([b4c85be](https://gitlab.com/5stones/scylla-xero/commit/b4c85be))


### Features

* **ToXeroTask:** Add some additional abstraction for easier extension of ToXeroTask ([6fb2ac6](https://gitlab.com/5stones/scylla-xero/commit/6fb2ac6))



# [1.1.0](https://gitlab.com/5stones/scylla-xero/compare/v1.0.2...v1.1.0) (2019-01-11)


### Features

* **XeroClient:** Add BatchPayments endpoint to client ([b3ac40d](https://gitlab.com/5stones/scylla-xero/commit/b3ac40d))



## [1.0.2](https://gitlab.com/5stones/scylla-xero/compare/v1.0.1...v1.0.2) (2018-11-28)


### Bug Fixes

* **requirements.txt, setup.py:** Add pycrypto dependency for private Xero apps ([d91197a](https://gitlab.com/5stones/scylla-xero/commit/d91197a))



## [1.0.1](https://gitlab.com/5stones/scylla-xero/compare/v1.0.0...v1.0.1) (2018-11-28)


### Bug Fixes

* **requirements.txt:** Fix requirements url ([ab18892](https://gitlab.com/5stones/scylla-xero/commit/ab18892))



# [1.0.0](https://gitlab.com/5stones/scylla-xero/compare/09601b3...v1.0.0) (2018-11-28)


### Bug Fixes

* **conversions:** make sure empty IDs are removed ([01557bf](https://gitlab.com/5stones/scylla-xero/commit/01557bf))
* **task:** fix paging of results, so it ends when less than 100 ([8d8385f](https://gitlab.com/5stones/scylla-xero/commit/8d8385f))
* **tasks:** fix error handling with submitting pages of records ([828a9d2](https://gitlab.com/5stones/scylla-xero/commit/828a9d2))
* **tasks:** prevent empty requests to xero ([09601b3](https://gitlab.com/5stones/scylla-xero/commit/09601b3))


### Features

* **app, client:** add all supported records and improve configuration ([cc63e15](https://gitlab.com/5stones/scylla-xero/commit/cc63e15)), closes [#1](https://gitlab.com/5stones/scylla-xero/issues/1)
* **app, tasks:** add ability to download xero records ([c47f116](https://gitlab.com/5stones/scylla-xero/commit/c47f116))
* **records:** add Invoice to linked records ([d937dc7](https://gitlab.com/5stones/scylla-xero/commit/d937dc7))
* **setup.*, LICENSE, README.md, package.json:** Add install files ([93d2f93](https://gitlab.com/5stones/scylla-xero/commit/93d2f93))
* **tasks:** improve error handling by saving validation errors ([931d0b9](https://gitlab.com/5stones/scylla-xero/commit/931d0b9))



